from django.apps import AppConfig
from xml.sax import ContentHandler, parse
import urllib.request

yt_videos = {}

class Processor(ContentHandler):

    def __init__(self):
        super().__init__()
        self.inEntry = False
        self.inContent = False
        self.Content = ""
        self.Title = ""

    def startElement(self, name, attrs):
        if name == 'entry':
            self.inEntry = True
        elif name == 'title' and self.inEntry:
            self.inContent = True
        elif name == 'link' and self.inEntry:
            self.Content += attrs.getValue('href')

    def endElement(self, name):
        if name == 'entry':
            self.handleEntry()
        elif name == 'title':
            self.handleTitle()

    def handleEntry(self):
        global yt_videos
        yt_videos[self.Title] = self.Content
        self.inEntry = False
        self.Title = ""
        self.Content = ""

    def handleTitle(self):
        self.inContent = False

    def characters(self, content):
        if self.inEntry and self.inContent:
            self.Title += content

class CmsConfig(AppConfig):
    name = "cms"

    def ready(self):
        url = "https://www.youtube.com/feeds/videos.xml?channel_id=UC300utwSVAYOoRLEqmsprfg"
        xmlStream = urllib.request.urlopen(url)
        parse(xmlStream, Processor())
        return yt_videos
