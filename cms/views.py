from django.shortcuts import render
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from . import apps

videos_selec = {}

@csrf_exempt
def index(request):
    if request.method == "POST":
        handle_post_request(request)
    return HttpResponse(render(request, 'cms/page.html',
                               {'selected_videos': videos_selec, 'not_selected_videos': apps.yt_videos}))

def handle_post_request(request):
    title = request.POST.get('title')
    url = request.POST.get('url')
    selection = request.POST.get('selection')

    if title and url and selection:
        if selection == 'yes':
            move_content(apps.yt_videos, videos_selec, title, url)
        elif selection == 'no':
            move_content(videos_selec, apps.yt_videos, title, url)

def move_content(origin, destiny, title, url):
    try:
        del origin[title]
        destiny[title] = url
    except KeyError:
        print(f'Key {title} does not exist')

def not_found(request, exception):
    return render(request, 'cms/not_found.html', status=404)